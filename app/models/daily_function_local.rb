class DailyFunctionLocal < ActiveRecord::Base
  belongs_to :daily
  belongs_to :function
  belongs_to :local
  
  attr_accessible :daily_id, :function_id, :local_id
end
