class CreateSemesters < ActiveRecord::Migration
  def change
    create_table :semesters do |t|
      t.string :name
      t.date :start_date
      t.date :end_date
      t.boolean :deleted

      t.timestamps
    end
  end
end
