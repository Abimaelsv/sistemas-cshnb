class CreateTravelDrivers < ActiveRecord::Migration
  def change
    create_table :travel_drivers do |t|
      t.references :travel
      t.references :driver
      t.integer :number_of_daily
      t.boolean :deleted
     

      t.timestamps
    end
    add_index :travel_drivers, :travel_id
    add_index :travel_drivers, :driver_id
  end
end
